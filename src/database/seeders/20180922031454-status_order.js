'use strict';

const sampleData = [{
  name: 'cancel',
},
{
  name: 'pending',
},
{
  name: 'process',
},
{
  name: 'done',
}
];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('status_orders', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('status_orders', null, {});
  }
};

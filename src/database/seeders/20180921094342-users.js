'use strict';
require('dotenv').config();
const faker = require('faker');
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let dataFaker = [];
    let password = await bcrypt.hash(
      process.env.BCRYPT_PASS,
      parseInt(process.env.BCRYPT_SALT)
    );
    for (let i = 1; i < 5; i++) {
      let objectFaker = {
        name: faker.name.findName(),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber('62821########'),
        password: `${password}`,
        is_admin: true,
        created_at: faker.date.past(),
        updated_at: faker.date.recent()
      };
      dataFaker.push(objectFaker);
    }
    for (let i = 1; i < 5; i++) {
      let objectFaker = {
        name: faker.name.findName(),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber('62821########'),
        password: `${password}`,
        is_chef: true,
        status: true,
        cooking_time: 2,
        created_at: faker.date.past(),
        updated_at: faker.date.recent()
      };
      dataFaker.push(objectFaker);
    }
    
    let superAdmin = {
      name: 'admin',
      email: 'panduakas@gmail.com',
      phone: '6282119322388',
      password: password,
      is_admin: true,
      created_at: new Date(),
      updated_at: new Date()
    };

    dataFaker.push(superAdmin);
    return queryInterface.bulkInsert('users', dataFaker, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};

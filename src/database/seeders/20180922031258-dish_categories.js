'use strict';
const faker = require('faker');

const sampleData = [{
  name: 'seafood',
  description: faker.lorem.sentence(),
  created_at: new Date(),
  updated_at: new Date()
},
{
  name: 'meat',
  description: faker.lorem.sentence(),
  created_at: new Date(),
  updated_at: new Date()
},
{
  name: 'drink',
  description: faker.lorem.sentence(),
  created_at: new Date(),
  updated_at: new Date()
}];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('dish_categories', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('dish_categories', null, {});
  }
};

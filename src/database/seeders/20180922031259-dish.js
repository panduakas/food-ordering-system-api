'use strict';
const faker = require('faker');

const sampleData = [{
  category_id: 1,
  name: 'Cumi Goreng Tepung',
  description: faker.lorem.sentence(),
  price: 15000,
  duration: 5,
  image_url: 'uploads/cumi-goreng-tepung.jpeg',
  created_at: new Date(),
  updated_at: new Date()
},
{
  category_id: 1,
  name: 'Udang Asam Manis',
  description: faker.lorem.sentence(),
  price: 20000,
  duration: 6,
  image_url: 'uploads/udang-asam-manis.jpeg',
  created_at: new Date(),
  updated_at: new Date()
},
{
  category_id: 2,
  name: 'Steak Lada Hitam',
  description: faker.lorem.sentence(),
  price: 25000,
  duration: 8,
  image_url: 'uploads/steak-lada-hitam.jpeg',
  created_at: new Date(),
  updated_at: new Date()
},
{
  category_id: 2,
  name: 'Steak Ayam Pedas',
  description: faker.lorem.sentence(),
  price: 23000,
  duration: 7,
  image_url: 'uploads/steak-ayam-pedas.jpeg',
  created_at: new Date(),
  updated_at: new Date()
},
{
  category_id: 3,
  name: 'Es Teh Manis',
  description: faker.lorem.sentence(),
  price: 2500,
  duration: 2,
  image_url: 'uploads/es-teh-manis.jpeg',
  created_at: new Date(),
  updated_at: new Date()
},
{
  category_id: 3,
  name: 'Es Teler',
  description: faker.lorem.sentence(),
  price: 3000,
  duration: 3,
  image_url: 'uploads/es-teler.jpeg',
  created_at: new Date(),
  updated_at: new Date()
}];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('dishes', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('dishes', null, {});
  }
};
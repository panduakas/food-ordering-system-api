const express = require('express');
const dish = require('./dish');
const orders = require('./orders');
const users = require('./users');
// Declare API Route and API Version
const v1 = express.Router();

// v1.use('/me', me);
v1.use('/users', users);
v1.use('/dish', dish);
v1.use('/orders', orders);

module.exports = v1;

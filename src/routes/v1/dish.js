const express = require('express');
const dishControllers = require('../../controllers/dish');
const route = express.Router();
const auth = require('../../helpers/v1/auth');

const upload = require('../../../config/multer');
const cpUpload = upload.single('image_url');


route.post('/', auth.checkAuth, cpUpload, dishControllers.createDish); // admin only, add new dish to database 
route.get('/', dishControllers.getAllDish); // everyone can access this
route.patch('/:dishId', auth.checkAuth, cpUpload, dishControllers.updateDish); //admin only, update data dish
route.delete('/:dishId', auth.checkAuth, dishControllers.deleteDish); //admin only, delete dish from database

module.exports = route;

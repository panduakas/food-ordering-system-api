const express = require('express');
const ordersControllers = require('../../controllers/orders');
const route = express.Router();
const auth = require('../../helpers/v1/auth');

route.post('/', auth.checkAuth, ordersControllers.createOrder); // create order only customer
route.delete('/:orderId', auth.checkAuth, ordersControllers.deleteOrder); // cancel order means delete the order (customer)
route.get('/', auth.checkAuth, ordersControllers.getApproximateTime); // check approximate wait time to customer
route.get('/status', auth.checkAuth, ordersControllers.getStatusOrder); // check customer order status
route.get('/next', auth.checkAuth, ordersControllers.nextOrder); // chef only, to get next dish for cook
route.patch('/', auth.checkAuth, ordersControllers.doneOrder); // chef only, if chef finish cook

module.exports = route;

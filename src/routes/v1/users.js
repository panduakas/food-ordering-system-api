const express = require('express');
const usersControllers = require('../../controllers/users');
const auth = require('../../helpers/v1/auth');
const route = express.Router();

route.post('/login', usersControllers.login); // everyone can access this end point to login
route.post('/register', usersControllers.register); // register only add data users customer
route.post('/admin', auth.checkAuth, usersControllers.createAdmin); // admin only, add another admin
route.post('/chef', auth.checkAuth, usersControllers.createChef); // admin only, add chef to database
route.patch('/logout', auth.checkAuth, usersControllers.logout); // everyone can access, update status login
route.get('/admin', auth.checkAuth, usersControllers.getAllAdmin); // admin only, show all admin available
route.get('/chef', auth.checkAuth, usersControllers.getAllChef); // admin only, show all chef available
route.delete('/:usersId', auth.checkAuth, usersControllers.deleteUsers); // admin only, delete user

module.exports = route;

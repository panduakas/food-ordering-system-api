const {dishes, dish_categories} = require('../../models');

const dishService = {
  getAll: async () => {
    const data = await dish_categories.findAll({
      include: [{
        model: dishes
      }]
    });
    return data;
  },
  createDish: async (category_id, name, description, duration, price, image_url) => {
    const data = await dishes.create({
      category_id,
      name,
      description,
      duration,
      price,
      image_url,
    });
    return data;
  },
  updateDishCategoryId: async (id) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({

    });
    return data;
  },
  updateDishName: async (id, category_id) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({category_id}, { fields: ['category_id']});
    return data;
  },
  updateDishDescription: async (id, description) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({description}, { fields: ['description']});
    return data;
  },
  updateDishDuration: async (id, duration) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({duration}, { fields: ['duration']});
    return data;
  },
  updateDishPrice: async (id, price) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({price}, { fields: ['price']});
    return data;
  },
  updateDishImage_url: async (id, image_url) => {
    const checkDish = await dishes.findById(id);
    const data = await checkDish.update({image_url}, { fields: ['image_url']});
    return data;
  },
  deleteDish: async (id) => {
    const data = await dishes.destroy({
      where: {
        id
      }
    });
    return data;
  },
  findDishById: async (id) => {
    const data = await dishes.findById(id);
    return data;
  }
};

module.exports = dishService;
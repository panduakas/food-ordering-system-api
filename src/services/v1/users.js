const {
  users
} = require('../../models');

const user = {
  updateStatus: async (id, status) => {
    const checkUser = await users.findById(id);
    const data = await checkUser.update({status}, { fields: ['status']});
    return data;
  },
  findUser: async (email, phone) => {
    const data = await users.findOne({
      where: {
        email,
        phone
      }
    });
    return data;
  },
  createUser: async (name, email, phone, password, is_admin, is_chef) => {
    const data = await users.create({
      name,
      email,
      phone,
      password,
      is_admin,
      is_chef
    });
    return data;
  },
  allAdmin: async () => {
    const data = await users.findAll({
      where: {
        is_admin: true
      }
    });
    return data;
  },
  allChef: async () => {
    const data = await users.findAll({
      where: {
        is_chef: true
      }
    });
    return data;
  },
  findUserById: async (id) => {
    const data = await users.findById(id);
    return data;
  },
  userDelete: async (id) => {
    const data = await users.destroy({
      where: {
        id
      }
    });
    return data;
  }
};

module.exports = user;

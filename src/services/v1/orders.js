const {
  orders,
  order_details,
  users,
  dishes,
  status_order
} = require('../../models');

const orderService = {
  approximateTime: async () => {
    const findChef = await users.findAll({
      where: {
        is_chef: true,
        status: true,
      }
    });
    let arrayTimeChef = [];
    if (findChef.length === 0) {
      arrayTimeChef.push(0);
    } else {
      for (let i = 0; findChef.length > i; i++) {
        arrayTimeChef.push(findChef[i].cooking_time);
      }
    }
    let delayTime = Math.max(...arrayTimeChef);

    const findOtherOrder = await orders.findAll({
      where: {
        status_id: 2
      }
    });

    totalDurationOther = []
    for (let i = 0; findOtherOrder.length > i; i++) {
      totalDurationOther.push(findOtherOrder[i].total_duration);
    }

    sumTotalDuration = 0;
    for (let i = 0; totalDurationOther.length > i; i++) {
      sumTotalDuration += totalDurationOther[i];
    }
    const approxTime = delayTime + sumTotalDuration;
    const upTime = approxTime - 2;

    return upTime;
  },
  createOrders: async (customer_id, listDish) => {
    let dataDish = [];
    let dataAmount = [];
    for (let i = 0; listDish.length > i; i++) {
      const findDish = await dishes.findOne({
        where: {
          id: listDish[i].dish_id
        }
      });
      dataAmount.push(listDish[i].amount);
      dataDish.push(findDish);
    }

    let dataDuration = [];
    let dataPrice = [];
    for (let i = 0; dataDish.length > i; i++) {
      dataPrice.push(dataDish[i].price * dataAmount[i]);
      dataDuration.push(dataDish[i].duration);
    }
    
    let totalDuration = 0 + dataAmount.length;
    let totalPrice = 0;
    for (let i = 0; dataPrice.length > i; i++) {
      totalPrice += dataPrice[i];
      totalDuration += dataDuration[i];
    }
    const findOtherOrder = await orders.findAll({
      where: {
        status_id: 2
      }
    });

    totalDurationOther = []
    for (let i = 0; findOtherOrder.length > i; i++) {
      totalDurationOther.push(findOtherOrder[i].total_duration);
    }

    sumTotalDuration = 0;
    for (let i = 0; totalDurationOther.length > i; i++) {
      sumTotalDuration += totalDurationOther[i];
    }

    const waitingTime = totalDuration + sumTotalDuration;

    const data = await orders.create({
      customer_id,
      total: totalPrice,
      status_id: 2,
      total_duration: totalDuration
    });

    const detail = [];
    for (const i of listDish) {
      const orderDetailCreate = await order_details.create({
        order_id: data.id,
        dish_id: i.dish_id,
        amount: i.amount,
        request: i.request
      });
      detail.push(orderDetailCreate);
    }

    data.detail = detail;
    return {
      data,
      detail,
      waitingTime
    };
  },
  statusOrder: async () => {
    const data = await status_order.findAll({
      include: [{
        model: orders
      }]
    });
    return data;
  },
  orderDelete: async id => {
    const data = await orders.findById(id);
    await data.update({
      where: {
        id: id
      },
      status_id: 1
    });
    return data;
  },
  findOrderById: async id => {
    const data = await orders.findById(id);
    return data;
  },
  pickOrders: async chef_id => {
    const checkChef = await users.findById(chef_id);
    const checkOrder = await orders.findOne({
      where: {
        status_id: 2
      }
    });

    const totalDuration = checkChef.cooking_time + checkOrder.total_duration;
    await checkChef.update({
      cooking_time: totalDuration - 2,
      order_id: checkOrder.id
    });
    const data = await checkOrder.update({
      chef_id: chef_id,
      status_id: 3
    });
    const detail = order_details.findAll({
      where: {
        order_id: checkOrder.id
      },
      include: [{
        model: order_details
      }]
    });
    return {data, detail};
  },
  doneOrders: async chef_id => {
    const checkChef = await users.findById(chef_id);
    const checkOrder = await orders.findOne({
      where: {
        chef_id: checkChef.id
      }
    });
    const totalDuration = checkChef.cooking_time - checkOrder.total_duration;
    await checkChef.update({
      cooking_time: totalDuration + 2,
      order_id: 0
    });
    const data = await checkOrder.update({
      chef_id: chef_id,
      status_id: 4
    });
    return data;
  }
};

module.exports = orderService;

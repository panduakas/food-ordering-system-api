const users = require('./users');
const dish = require('./dish');
const orders = require('./orders');

module.exports = { users, dish, orders };

const {
  response
} = require('../helpers/v1/response');
const {
  orders
} = require('../services/v1');

const createOrder = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (req.user.is_admin || req.user.is_chef) {
      throw new Error('Only Customer can access this endpoint!');
    }
    
    const orderCreate = await orders.createOrders(req.user.id, req.body);
    if (!orderCreate) {
      throw new Error('Failed to create order');
    } else {
      res.status(200).json(response(true, 'Order has been successfully created', orderCreate));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};
const deleteOrder = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (req.user.is_admin || req.user.is_chef) {
      throw new Error('Only Customer can access this endpoint!');
    }
    const orderFind = await orders.findOrderById(req.params.orderId);
  
    if (orderFind.chef_id !== null) {
      throw new Error('Cant cancel this order because chef already process it.')
    }
    if (orderFind.customer_id !== req.user.id) {
      throw new Error('You cant do that! this orders isnt yours.')
    }
    const orderDel = await orders.orderDelete(req.params.orderId);
    if (!orderDel) {
      throw new Error('Failed to delete order');
    } else {
      res.status(200).json(response(true, 'Order has been successfully canceled', orderDel));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const getApproximateTime = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (req.user.is_admin || req.user.is_chef) {
      throw new Error('Only Customer can access this endpoint!');
    }

    const getTimeApprox = await orders.approximateTime();
    if (!getTimeApprox) {
      res.status(200).json(response(true, 'No waiting time.'));
    } else {
      res.status(200).json(response(true, 'Waiting time is', getTimeApprox));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const getStatusOrder = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (req.user.is_admin || req.user.is_chef) {
      throw new Error('Only Customer can access this endpoint!');
    }
    const getStatusOrders = await orders.statusOrder();
    if (!getStatusOrders) {
      throw new Error('Failed to create order');
    } else {
      res.status(200).json(response(true, 'Order has been successfully retrieved', getStatusOrders));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const nextOrder = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_chef) {
      throw new Error('Only Chef can access this endpoint!');
    }
    const orderPick = await orders.pickOrders(req.user.id);
    if (!orderPick) {
      throw new Error('Failed to pick order');
    } else {
      res.status(200).json(response(true, 'Pick order success!', orderPick));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};
const doneOrder = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_chef) {
      throw new Error('Only Chef can access this endpoint!');
    }
    const orderDone = await orders.doneOrders(req.user.id);
    if (!orderDone) {
      throw new Error('Failed to set status done order');
    } else {
      res.status(200).json(response(true, 'Order done!', orderDone));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};
module.exports = {
  createOrder,
  deleteOrder,
  nextOrder,
  doneOrder,
  getStatusOrder,
  getApproximateTime
};

const {
  response
} = require('../helpers/v1/response');
const {
  dish
} = require('../services/v1');

const createDish = async (req, res) => {
  const {
    category_id,
    name,
    description,
    duration,
    price
  } = req.body;
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const addNewDish = await dish.createDish(category_id, name, description, duration, price, 'uploads/' + req.file.originalname);
    if (!addNewDish) {
      throw new Error('Failed to add a new dish');
    } else {
      return res.status(200).json(response(true, 'Successfully to add a new dish', addNewDish));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const getAllDish = async (req, res) => {
  try {
    const showDish = await dish.getAll();
    if (!showDish) {
      throw new Error('Failed to show dish list');
    } else {
      res.status(200).json(response(true, 'Dish list successfully retrieved', showDish));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const updateDish = async (req, res) => {
  const {
    category_id,
    name,
    description,
    duration,
    price
  } = req.body;
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }

    const updateDishCategoryId = await dish.updateDishCategoryId(req.params.dishId, category_id);
    const updateDishName = await dish.updateDishName(req.params.dishId, name);
    const updateDishDecription = await dish.updateDishDescription(req.params.dishId, description);
    const updateDishDuration = await dish.updateDishDuration(req.params.dishId, duration);
    const updateDishPrice = await dish.updateDishPrice(req.params.dishId, price);
    const updateDishImageUrl = await dish.updateDishImage_url(req.params.dishId, 'uploads/' + req.file.originalname);
    const currentData = await dish.findDishById(req.params.dishId);
    if (!updateDishCategoryId || !updateDishName || !updateDishDecription || !updateDishDuration || !updateDishPrice || !updateDishImageUrl) {
      throw new Error('Failed update dish');
    } else {
      res.status(200).json(response(true, 'Success update dish', currentData));
    }

  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const deleteDish = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const currentData = await dish.findDishById(req.params.dishId);
    const destroyDish = await dish.deleteDish(req.params.dishId);
    if (!destroyDish) {
      throw new Error('Failed to delete dish');
    } else {
      return res.status(200).json(response(true, 'Successfully delete dish', currentData));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

module.exports = {
  createDish,
  getAllDish,
  updateDish,
  deleteDish
};

const {
  response
} = require('../helpers/v1/response');
const {
  users
} = require('../services/v1');
require('dotenv').config();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bcrypt = require('bcrypt');

const login = (req, res, next) => {
  passport.authenticate(
    'local', {
      session: false
    },
    async (err, user, info) => {
      try {
        if (err) {
          return res.status(400).next(err);
        }
        if (!user) {
          return res.status(400).json({
            message: info.message
          });
        }
        const data = {
          email: user.email,
          created_at: new Date()
        };
        const token = jwt.sign(data, process.env.JWT_SECRET, {
          expiresIn: 86400 * 7
        });
        const included = {
          token: token
        };
        const statusUpdate = await users.updateStatus(user.id, true);
        if (!statusUpdate) {
          throw new Error('Failed to update status login');
        } else {
          return res
            .status(200)
            .json(response(true, 'User signed in', user, included));
        }
      } catch (error) {
        return res.status(400).json(response(false, error.message));
      }
    }
  )(req, res, next);
};

const register = async (req, res) => {
  const {
    name,
    email,
    phone,
    password,
    password2
  } = req.body;
  try {
    const checkUser = await users.findUser(email, phone);
    if (checkUser) {
      throw new Error('Email or Phone already exist!');
    }
    if (password !== password2) {
      throw new Error('Password comfirmation does not match');
    }
    const newPassword = await bcrypt.hash(password, 10);
    const userCreate = await users.createUser(name, email, phone, newPassword);
    if (!userCreate) {
      throw new Error('Failed to create a new customer');
    } else {
      res.status(200).json(response(true, 'Registration Success!', userCreate));
    }
  } catch (error) {
    return res.status(400).json(response(false, error.message));
  }
};

const createAdmin = async (req, res) => {
  const {
    name,
    email,
    phone,
    password,
    is_admin
  } = req.body;
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const newPassword = await bcrypt.hash(password, 10);
    const adminCreate = await users.createUser(name, email, phone, newPassword, is_admin);
    if (!adminCreate) {
      throw new Error('Failed to create a new administrator');
    } else {
      res.status(200).json(response(true, 'Create a new administrator', adminCreate));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const createChef = async (req, res) => {
  const {
    name,
    email,
    phone,
    password,
    is_admin,
    is_chef
  } = req.body;
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const newPassword = await bcrypt.hash(password, 10);
    const chefCreate = await users.createUser(name, email, phone, newPassword, is_admin, is_chef);
    if (!chefCreate) {
      throw new Error('Failed to create a new chef');
    } else {
      res.status(200).json(response(true, 'Create a new chef', chefCreate));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const getAllAdmin = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const showAdmin = await users.allAdmin();
    if (!showAdmin) {
      throw new Error('Failed to show all admin.');
    } else {
      res.status(200).json(response(true, 'All admin successfully retrieved.', showAdmin));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};
const getAllChef = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const showChef = await users.allChef();
    if (!showChef) {
      throw new Error('Failed to show all chef.');
    } else {
      res.status(200).json(response(true, 'All chef successfully retrieved.', showChef));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const logout = async (req, res) => {
  try {
    if (!req.user) {
      throw new Error('Login first!');
    }
    const statusUpdate = await users.updateStatus(req.user.id, false);
    if (!statusUpdate) {
      throw new Error('Failed to update status login');
    } else {
      return res
        .status(200)
        .json(response(true, 'User successfully logout', req.user ));
    }

  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const deleteUsers = async (req, res) => {
  try {
    if (req.user.status === false) {
      throw new Error('Login first!');
    }
    if (!req.user.is_admin) {
      throw new Error('Only Administrator can access this endpoint!');
    }
    const dataUser = await users.findUserById(req.params.usersId);
    const destroyUser = await users.userDelete(req.params.usersId);
    if (!destroyUser) {
      throw new Error('Failed to delete user');
    } else {
      res.status(200).json(response(true, 'Successfully delete user', dataUser));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

module.exports = {
  login,
  logout,
  register,
  createAdmin,
  createChef,
  getAllAdmin,
  getAllChef,
  deleteUsers
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  let users = sequelize.define(
    'users',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Invalid email!'
          },
          notEmpty: {
            msg: 'Email is required!'
          }
        }
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          len: {
            args: [10, 12],
            msg: 'Phone number is invalid! (10 - 12) characters'
          },
          not: {
            args: ['[a-z]', 'i'],
            msg: 'Phone number is invalid!'
          }
        }
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      is_admin: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      is_chef: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      cooking_time: {
        type: DataTypes.BOOLEAN,
      },
      created_at: {
        type: DataTypes.DATE
      },
      updated_at: {
        type: DataTypes.DATE
      }
    },
    {
      timestamp: true,
      underscored: true
    }
  );
  users.associate = function(models) {
    users.hasMany(models.orders, {
      foreignKey: 'customer_id'
    });
    users.hasMany(models.orders, {
      foreignKey: 'chef_id'
    });
  };
  return users;
};

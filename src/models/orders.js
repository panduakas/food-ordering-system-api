'use strict';
module.exports = (sequelize, DataTypes) => {
  const orders = sequelize.define(
    'orders',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      chef_id: {
        allowNull: true,
        foreignKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      customer_id: {
        allowNull: false,
        foreignKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      total: {
        type: DataTypes.INTEGER
      },
      status_id: {
        type: DataTypes.INTEGER
      },
      total_duration: {
        type: DataTypes.INTEGER
      },
      created_at: {
        type: DataTypes.DATE
      },
      updated_at: {
        type: DataTypes.DATE
      }
    },
    {
      timestamps: true,
      underscored: true
    }
  );
  orders.associate = function(models) {
    // associations can be defined here
    orders.belongsTo(models.users, {
      foreignKey: 'chef_id'
    });
    orders.belongsTo(models.users, {
      foreignKey: 'customer_id'
    });
    orders.hasMany(models.order_details, {
      foreignKey: 'order_id',
      onDelete: 'cascade',
      hooks: true
    });
    orders.belongsTo(models.status_order, {
      foreignKey: 'status_id'
    });
  };
  return orders;
};

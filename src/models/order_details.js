'use strict';
module.exports = (sequelize, DataTypes) => {
  const order_details = sequelize.define(
    'order_details',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      order_id: {
        allowNull: false,
        foreignKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'orders',
          key: 'id'
        }
      },
      dish_id: {
        allowNull: false,
        foreignKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'dishes',
          key: 'id'
        }
      },
      amount: {
        type: DataTypes.STRING
      },
      request: {
        type: DataTypes.TEXT
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE
      }
    },
    {
      timestamps: true, // auto timestamp in created_at and updated_at
      underscored: true // snake_case column
    }
  );
  order_details.associate = function(models) {
    // associations can be defined here
    order_details.belongsTo(models.dishes, {
      foreignKey: 'dish_id'
    });
    order_details.belongsTo(models.orders, {
      foreignKey: 'order_id'
    });
  };
  return order_details;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const orders = sequelize.define(
    'status_order',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false,
      underscored: true
    }
  );
  orders.associate = function(models) {
    // associations can be defined here
    orders.hasMany(models.orders, {
      foreignKey: 'status_id'
    });
  };
  return orders;
};

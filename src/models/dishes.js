'use strict';
module.exports = (sequelize, DataTypes) => {
  let dish = sequelize.define(
    'dishes',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      category_id: {
        allowNull: false,
        foreignKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'dish_categories',
          key: 'id'
        }
      },
      name: {
        type: DataTypes.STRING
      },
      description: {
        type: DataTypes.TEXT
      },
      duration: {
        type: DataTypes.INTEGER
      },
      price: {
        type: DataTypes.INTEGER
      },
      image_url: {
        type: DataTypes.STRING
      },
      created_at: {
        type: DataTypes.DATE
      },
      updated_at: {
        type: DataTypes.DATE
      }
    },
    {
      timestamps: true,
      underscored: true
    }
  );
  dish.associate = function(models) {
    // associations can be defined here
    dish.belongsTo(models.dish_categories, {
      foreignKey: 'category_id'
    });

    dish.hasMany(models.order_details, {
      foreign_key: 'dish_id',
      onDelete: 'cascade',
      hooks: true,
    });
  };
  return dish;
};

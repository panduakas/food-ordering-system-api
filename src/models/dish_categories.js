'use strict';
module.exports = (sequelize, DataTypes) => {
  let dishCategory = sequelize.define(
    'dish_categories',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.TEXT
      },
      created_at: {
        type: DataTypes.DATE
      },
      updated_at: {
        type: DataTypes.DATE
      }
    },
    {
      timestamp: true,
      underscored: true
    }
  );
  dishCategory.associate = function(models) {
    // associations can be defined here
    dishCategory.hasMany(models.dishes, {
      foreignKey: 'category_id',
      onDelete: 'cascade',
      hooks: true
    });
  };
  return dishCategory;
};
